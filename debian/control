Source: watcher
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 po-debconf,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-apscheduler,
 python3-cinderclient,
 python3-coverage,
 python3-croniter,
 python3-freezegun,
 python3-futurist,
 python3-glanceclient,
 python3-gnocchiclient,
 python3-hacking,
 python3-ironicclient,
 python3-jsonpatch,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-lxml,
 python3-microversion-parse,
 python3-monascaclient,
 python3-networkx,
 python3-neutronclient,
 python3-novaclient,
 python3-openstackclient,
 python3-openstackdocstheme,
 python3-os-api-ref,
 python3-os-resource-classes,
 python3-oslo.cache,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging (>= 14.1.0),
 python3-oslo.policy,
 python3-oslo.reports,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-oslo.versionedobjects,
 python3-oslotest,
 python3-pastedeploy,
 python3-pecan,
 python3-prettytable,
 python3-sphinxcontrib-pecanwsme,
 python3-sphinxcontrib.apidoc,
 python3-sqlalchemy,
 python3-stestr,
 python3-stevedore,
 python3-taskflow,
 python3-testscenarios,
 python3-testtools,
 python3-webob,
 python3-webtest,
 python3-wsme,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/watcher
Vcs-Git: https://salsa.debian.org/openstack-team/services/watcher.git
Homepage: https://github.com/openstack/watcher

Package: python3-watcher
Section: python
Architecture: all
Depends:
 python3-apscheduler,
 python3-cinderclient,
 python3-croniter,
 python3-futurist,
 python3-glanceclient,
 python3-gnocchiclient,
 python3-ironicclient,
 python3-jsonpatch,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-lxml,
 python3-microversion-parse,
 python3-monascaclient,
 python3-networkx,
 python3-neutronclient,
 python3-novaclient,
 python3-openstackclient,
 python3-os-resource-classes,
 python3-oslo.cache,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.messaging (>= 14.1.0),
 python3-oslo.policy,
 python3-oslo.reports,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-oslo.versionedobjects,
 python3-pastedeploy,
 python3-pbr,
 python3-pecan,
 python3-prettytable,
 python3-sqlalchemy,
 python3-stevedore,
 python3-taskflow,
 python3-webob,
 python3-wsme,
 ${misc:Depends},
 ${python3:Depends},
Conflicts:
 python-watcher,
Description: OpenStack Cloud Optimization as a Service - Python libraries
 OpenStack Watcher provides a flexible and scalable resource optimization
 service for multi-tenant OpenStack-based clouds. Watcher provides a complete
 optimization loop-including everything from a metrics receiver, complex event
 processor and profiler, optimization processor and an action plan applier.
 This provides a robust framework to realize a wide range of cloud optimization
 goals, including the reduction of data center operating costs, increased
 system performance via intelligent virtual machine migration, increased energy
 efficiency-and more!

Package: watcher-api
Architecture: all
Depends:
 adduser,
 debconf,
 python3-keystoneclient,
 python3-openstackclient,
 q-text-as-data,
 watcher-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Cloud Optimization as a Service - API server
 OpenStack Watcher provides a flexible and scalable resource optimization
 service for multi-tenant OpenStack-based clouds. Watcher provides a complete
 optimization loop-including everything from a metrics receiver, complex event
 processor and profiler, optimization processor and an action plan applier.
 This provides a robust framework to realize a wide range of cloud optimization
 goals, including the reduction of data center operating costs, increased
 system performance via intelligent virtual machine migration, increased energy
 efficiency-and more!
 .
 This package contains the Watcher API server.

Package: watcher-applier
Architecture: all
Depends:
 watcher-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Cloud Optimization as a Service - Applier
 OpenStack Watcher provides a flexible and scalable resource optimization
 service for multi-tenant OpenStack-based clouds. Watcher provides a complete
 optimization loop-including everything from a metrics receiver, complex event
 processor and profiler, optimization processor and an action plan applier.
 This provides a robust framework to realize a wide range of cloud optimization
 goals, including the reduction of data center operating costs, increased
 system performance via intelligent virtual machine migration, increased energy
 efficiency-and more!
 .
 This package contains the Watcher Applier.

Package: watcher-common
Architecture: all
Depends:
 adduser,
 dbconfig-common,
 debconf,
 python3-watcher (= ${binary:Version}),
 sqlite3,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Cloud Optimization as a Service - common files
 OpenStack Watcher provides a flexible and scalable resource optimization
 service for multi-tenant OpenStack-based clouds. Watcher provides a complete
 optimization loop-including everything from a metrics receiver, complex event
 processor and profiler, optimization processor and an action plan applier.
 This provides a robust framework to realize a wide range of cloud optimization
 goals, including the reduction of data center operating costs, increased
 system performance via intelligent virtual machine migration, increased energy
 efficiency-and more!
 .
 This package contains common files and configuration that are
 needed by all the daemon packages of Watcher.

Package: watcher-decision-engine
Architecture: all
Depends:
 watcher-common (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Cloud Optimization as a Service - Decision Engine
 OpenStack Watcher provides a flexible and scalable resource optimization
 service for multi-tenant OpenStack-based clouds. Watcher provides a complete
 optimization loop-including everything from a metrics receiver, complex event
 processor and profiler, optimization processor and an action plan applier.
 This provides a robust framework to realize a wide range of cloud optimization
 goals, including the reduction of data center operating costs, increased
 system performance via intelligent virtual machine migration, increased energy
 efficiency-and more!
 .
 This package contains the Watcher decision engine.

Package: watcher-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack Cloud Optimization as a Service - doc
 OpenStack Watcher provides a flexible and scalable resource optimization
 service for multi-tenant OpenStack-based clouds. Watcher provides a complete
 optimization loop-including everything from a metrics receiver, complex event
 processor and profiler, optimization processor and an action plan applier.
 This provides a robust framework to realize a wide range of cloud optimization
 goals, including the reduction of data center operating costs, increased
 system performance via intelligent virtual machine migration, increased energy
 efficiency-and more!
 .
 This package contains the documentation.
